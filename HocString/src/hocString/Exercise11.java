package hocString;

//Write a Java program to create a new String object with the contents of a character array.
public class Exercise11 {
	public static void main(String[] args) {
		char[] arr = new char[] { '1', '2', '3', '4','5' };
		String str = String.copyValueOf(arr, 0, 3);
		System.out.println(str);
	}
}
