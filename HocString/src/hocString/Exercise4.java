package hocString;
//Write a java program to count a number of Unicode code points in the specified text range of a String.
public class Exercise4 {
	public static void main(String[] args) {
	    String str = "Tran Viet Son";
	    System.out.println("String is : " + str);
	    int ctr = str.codePointCount(0,8);
	    System.out.println(ctr);
	  }
}
