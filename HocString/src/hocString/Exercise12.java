package hocString;

public class Exercise12 {
	public static void main(String[] args) {
		String str1 = "Tran Viet Son1";
		String str2 = "Tran Viet Son";
		boolean ends1 = str1.endsWith("on");
		boolean ends2 = str2.endsWith("on");

		System.out.println(ends1);//false
		System.out.println(ends2);//true
	}
}
