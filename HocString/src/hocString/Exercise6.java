package hocString;
//Write a java program to compare two strings lexicographically, ignoring case differences.
public class Exercise6 {
	public static void main(String[] args) {
		String str1 = "Tran Viet Son 1";
		String str2 = "Tran Viet Son 0";
		// String str2 = "Tran Viet Son 1";
		// String str2 = "Tran Viet Son 2";
		// test 3 test case

		System.out.println("String 1 is : " + str1);
		System.out.println("String 2 is: " + str2);
		int result = str1.compareToIgnoreCase(str2);
		if (result < 0) {
			System.out.println(str1 + " is less than " + str2);
		} else if (result == 0) {
			System.out.println(str1 + " is equal to " + str2);
		} else // if (result > 0)
		{
			System.out.println(str1 + " is greater than " + str2);
		}
	}
}
