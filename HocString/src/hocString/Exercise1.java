package hocString;
//Write a Java program to get the character at the given index within the String. 
public class Exercise1 {
	public static void main(String[] args)
    {
        String st = "Tran Viet Son";
        System.out.println("String is = " + st);
        int index1 = st.charAt(0);
        int index2 = st.charAt(10);

       
        System.out.println("Vi tri  0 is " +
            (char)index1);
        System.out.println("Vi tri  10 is " +
            (char)index2);
    }
}
