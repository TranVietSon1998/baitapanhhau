package hocString;
////Write a Java program to check whether two String objects contain the same data.
public class Exercise13 {
	public static void main(String[] args)
    {
        String str1 = "tRAN vIET Son";
        String str2 = "Lam Cong Hau";
        String str3 = "Nguyen Tan Hau";
        String str4 = "Nguyen Tan Hau";
        boolean eq1 = str1.equals(str2);
        boolean eq2 = str1.equals(str3);
        boolean eq3 = str3.equals(str4);
        System.out.println(eq1);//f
        System.out.println(eq2);//f
        System.out.println(eq3);//t
       
    }
}
