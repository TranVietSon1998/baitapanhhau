package hocString;
//Write a Java program to test if a given string contains the specified sequence of char values.
public class Exercise8 {
	public static void main(String[] args)
    {
        String str1 = "Tran Viet Son ";
        String str2 = "Son";
        System.out.println(str1.contains(str2));
    }
}
