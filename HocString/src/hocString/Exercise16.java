package hocString;
//Write a Java program to get the contents of a given string as a byte array.
public class Exercise16 {
	public static void main(String[] args) {
        String str = "Tran Viet SOn";
        byte[] byte_arr = str.getBytes();
        String new_str = new String(byte_arr);
        System.out.println(str+" eq " +new_str);
    }
}
