package hocString;
//Write a Java program to get the character (Unicode code point) at the given index within the String
public class Exercise2 {
	public static void main(String[] args) {
	    String str = "Tran Viet Son";
	    System.out.println("String is : " + str);
	    int value1 = str.codePointAt(1);
	    int value2 = str.codePointAt(9);
	    System.out.println( value1);
	    System.out.println( value2);
	  }
}
