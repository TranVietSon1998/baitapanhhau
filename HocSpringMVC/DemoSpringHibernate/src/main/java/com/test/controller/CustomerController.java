package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.test.entities.Customer;
import com.test.service.CustomerService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = {"/"})
	public String listCustomer(Model model) {
		model.addAttribute("listCustomer", customerService.findAll());
		return "index";
	}

	@PostMapping(value = "/add")
	public String add(HttpServletRequest request) throws IOException {
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		Customer customer = new Customer(name, address);
		customerService.save(customer);
		return "redirect:/";
	}
	@PostMapping(value = "/update")
	public String update(HttpServletRequest request) throws IOException{
		int id=Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		Customer customer=customerService.findById(id);
		customer.setName(name);
		customer.setAddress(address);
		customerService.update(customer);
		return "redirect:/";
	}

	@GetMapping("/update")
	public String update(HttpServletRequest request, Model model) throws IOException{
		int id=Integer.parseInt(request.getParameter("id"));
		 Customer customer=customerService.findById(id);
		model.addAttribute("customer", customer);
		return "editCustomer";
	}


	@GetMapping("/add")
	public String add() {
		return "AddCustomer";
	}

	@GetMapping(value = "/delete")
	public String delete(HttpServletRequest request) {
		String id = request.getParameter("id");
		customerService.delete(id);
		return "redirect:/";
	}

	@PostMapping(value = "/search")
	public String search(HttpServletRequest request, Model model){
//		int id=Integer.parseInt(request.getParameter("id"));
		String name=request.getParameter("name");

//		Customer customer=customerService.findById(id);
		Customer customer = customerService.findByName(name);
		model.addAttribute("customer", customer);


		return "search";
	}




}
