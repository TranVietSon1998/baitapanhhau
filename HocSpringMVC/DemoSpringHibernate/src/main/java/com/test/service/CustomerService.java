package com.test.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.dao.CustomerDAO;
import com.test.entities.Customer;

@Service
@Transactional
public class CustomerService {

	@Autowired
	private CustomerDAO customerDAO;

	public List<Customer> findAll() {
		return customerDAO.findAll();
	}

	public Customer findById(final int id) {
		return customerDAO.findById(id);
	}

	public Customer findByName(final String name){
		return  customerDAO.findByName(name);
	}


	public void save(Customer customer) {
		customerDAO.save(customer);
	}

	public void delete(String id) {
		Customer customer = customerDAO.findById(Integer.parseInt(id));
		customerDAO.delete(customer);
	}
	public void update(Customer customer){
		customerDAO.update(customer);
	}
}
