package com.test.controller;

import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;

@Controller
public class controller {
    @GetMapping("/")
    public String calculator(){
        return "index";
    }
    @PostMapping("/")
    public String calculator(HttpServletRequest request, Model model){
        int n1=Integer.parseInt(request.getParameter("n1"));
        int n2=Integer.parseInt(request.getParameter("n2"));
        String operator=request.getParameter("operator");
        String  result="";
        if(operator.equalsIgnoreCase("+")){
            result="ket qua la :"+(n1+n2);
        }
        else if(operator.equalsIgnoreCase("-")){
             result="ket qua la :"+(n1-n2);
        }
        else if(operator.equalsIgnoreCase("x")|| operator.equalsIgnoreCase("*")){
            result="ket qua la :"+(n1*n2);
        }
        else if(operator.equalsIgnoreCase(":")|| operator.equalsIgnoreCase("/")){
            if(n2==0){
                result="phep toan k hop le";
            }
            else{
                result="ket qua la "+(n1/n2);
            }

        }
        request.setAttribute("result",result);
        request.setAttribute("n1",n1);
        request.setAttribute("n2",n2);
        request.setAttribute("operator",operator);




        return "index";
    }


}
